# vac_ctrl_mks946_937b conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/vac_ctrl_mks946_937b"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS vac_ctrl_mks946_937b module
